<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'change_password']);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $input = $request->all();

        $user = \App\User::where([
            'username' => $input['username'],
            'password' => $input['password']
        ])->first();

        if ($user) {
            Auth::loginUsingId($user->id_user);

            \App\LoginLogs::create([
                'time' => date('Y-m-d H:i:s'),
                'ip' => $request->ip(),
                'id_user' => $user->id_user,
            ]);

            if (Auth::user()->level == 0) {
                return redirect('/guru');
            }
            return redirect('/siswa');
        }
        return redirect('/login');
    }

    public function change_password(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required|max:150',
            'conf_password' => 'required|max:150',
        ]);
        $input = $request->all();

        $get_user = \App\User::where('id_user', Auth::user()->id_user);
        $user = $get_user->first();

        if ($input['old_password'] == $user->id_user) {
            if ($input['new_password'] == $input['conf_password']) {
                $user->update([
                    'password' => $input['new_password']
                ]);
            }
        }

        if (Auth::user()->level == 0) {
            return redirect('/guru');
        }
        return redirect('/siswa');
    }
}
