<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;

class SiswaController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('siswa');
	}

	public function index()
	{
		return view('siswa.home');
	}

	public function answer_exam()
	{
		return view('siswa.answer_exam');
	}

	public function answer_question($id='')
	{
		return view('siswa.answer_question', [
			'data' => \App\Soal::where('id_soal', $id)->first()
		]);
	}

	public function store_answer($id='', Request $request)
	{
		$input = $request->all();
		$data_jawab = [
			'id_user' => Auth::user()->id_user,
			'id_soal' => $id
		];
		$jawab = \App\Jawab::create($data_jawab);
		foreach ($input['answer'] as $key => $value) {
			$data = [
				'answer' => $value,
				'id_detail_soal' => $key,
				'id_jawab' => $jawab->id_jawab
			];
			\App\DetailJawab::create($data);
		}
		return redirect('/answer_exam');
	}

	public function view_score()
	{
		return view('siswa.view_score');	
	}
}
