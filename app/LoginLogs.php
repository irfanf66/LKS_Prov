<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginLogs extends Model
{
    protected $table = 'login_logs';
    protected $primaryKey = 'id_logs';
    protected $fillable = [
    	'time', 'ip', 'id_user'
    ];
    public $timestamps = false;
}
