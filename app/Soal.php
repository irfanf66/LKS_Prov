<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $table = 'soal';
    protected $primaryKey = 'id_soal';
    protected $fillable = [
    	'nama_soal', 'start', 'end', 'id_kelas'
    ];

    public $timestamps = false;

    public function kelas()
    {
    	return $this->hasOne('App\Kelas', 'id_kelas', 'id_kelas');
    }

    public function detail_soal()
    {
    	return $this->hasMany('App\DetailSoal', 'id_soal', 'id_soal');
    }
}
