<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailSoal extends Model
{
    protected $table = 'detail_soal';
    protected $primaryKey = 'id_detail_soal';
    protected $fillable = [
    	'question',
    	'optionA',
    	'optionB',
    	'optionC',
    	'optionD',
    	'optionE',
    	'kj',
    	'tipe',
    	'weight',
    	'id_soal',
    ];

    public $timestamps = false;

    public function soal()
    {
    	return $this->hasOne('App\Soal', 'id_soal', 'id_soal');
    }
}
