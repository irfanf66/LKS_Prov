<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawab extends Model
{
    protected $table = 'jawab';
    protected $primaryKey = 'id_jawab';
    protected $fillable = [
    	'nilai',
    	'id_user',
    	'id_soal',
    ];

    public $timestamps = false;

    public function user()
    {
    	return $this->hasOne('App\User', 'id_user', 'id_user');
    }

    public function soal()
    {
    	return $this->hasOne('App\Soal', 'id_soal', 'id_soal');
    }

    public function detail_jawab()
    {
    	return $this->hasMany('App\DetailJawab', 'id_jawab', 'id_jawab');
    }
}
