<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::Routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', function () {
    return redirect('/login');
});
Route::post('/change_password', 'Auth\LoginController@change_password');

Route::get('/guru', 'GuruController@index');
Route::get('/create_exam', 'GuruController@create_exam');
Route::post('/store_exam', 'GuruController@store_exam');
Route::get('/manage_exam/{id}', 'GuruController@manage_exam');
Route::get('/delete_exam/{id}', 'GuruController@delete_exam');
Route::post('/store_question/{id}/{tipe}', 'GuruController@store_question');
Route::get('/delete_question/{id}', 'GuruController@delete_question');

Route::get('/assess_exam', 'GuruController@assess_exam');
Route::get('/assess_exam/{id}', 'GuruController@assess_exam_id');
Route::get('/assess_question/{id}', 'GuruController@assess_question');
Route::post('/store_score/{id}', 'GuruController@store_score');

Route::get('/statistic', 'GuruController@statistic');
Route::get('/statistic/{id}', 'GuruController@statistic_detail');


Route::get('/siswa', 'SiswaController@index');
Route::get('/answer_exam', 'SiswaController@answer_exam');
Route::get('/answer_question/{id}', 'SiswaController@answer_question');
Route::post('/store_answer/{id}', 'SiswaController@store_answer');

Route::get('/view_score', 'SiswaController@view_score');
