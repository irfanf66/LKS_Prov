@extends('layouts.app')
@section('content')
<div class="table-wrapper">
	<h3>12A Rank</h3>
	<table border="1">
		<tr>
			<th>Rank</th>
			<th>Student ID</th>
			<th>Student Name</th>
			<th>Average Score</th>
		</tr>
		@php
		if ($id == 'all')
			$data = \App\Siswa::all();
		else
			$data = \App\Siswa::where('id_kelas', $id)->get();

		$x = [];
		$f = [];
		foreach ($data as $key => $val) {
			$nilai = \App\Jawab::where('id_user', $val->id_user)->avg('nilai');
			$val->nilai = $nilai ? $nilai : 0;
			$x[$key] = $val->nilai;
			$val->toArray();
			$f[] = $val->toArray();
		}

		array_multisort($x, SORT_DESC, $f);

		@endphp
		@foreach ($f as $g)
		<tr>
			<td>{{ $i = !empty($i) ? $i+1 : 1 }}</td>
			<td>{{ $g['nis'] }}</td>
			<td>{{ $g['nama_siswa'] }}</td>
			<td>{{ $g['nilai'] }}</td>
		</tr>
		@endforeach
	</table>
</div>
@endsection