@extends('layouts.app')
@section('content')

<div class="exam-data">
<div>{{ $data->nama_soal }} kelas {{ $data->kelas->nama_kelas }}</div>
<div>{{ date("d F Y H:i:s", strtotime($data->start)) }} - {{ date("d F Y H:i:s", strtotime($data->end)) }}</div>
</div>
<div class="multiple-choice-wrapper">
<h3 style="text-align:center">Multiple Choice</h3>
<div class="form-wrapper">
	<form action="{{ url('store_question/' . $data->id_soal . '/pg') }}" method="POST">
		{{ csrf_field() }}
		<div>
			<input type="text" name="question" placeholder="Question">
		</div>

		<div>
			<input type="text" name="optionA" placeholder="optionA">
		</div>
		<div>
			<input type="text" name="optionB" placeholder="optionB">
		</div>
		<div>
			<input type="text" name="optionC" placeholder="optionC">
		</div>
		<div>
			<input type="text" name="optionD" placeholder="optionD">
		</div>
		<div>
			<input type="text" name="optionE" placeholder="optionE">
		</div>
		<div>
			<input type="number" name="weight" placeholder="Score Weight">
		</div>

		<div>
			<select name="kj">
				<option selected>--Select Correct Choice--</option>
				<option value="a">A</option>
				<option value="b">B</option>
				<option value="c">C</option>
				<option value="d">D</option>
				<option value="e">E</option>
			</select>
		</div>
		<div>
			<input type="submit" value="Insert Question" class="button">
		</div>
	</form>
</div>
<div>
	<div class="multiple-choice">
		@foreach ($data->detail_soal as $value)
		@if ($value->tipe == 'pg')
		<div class="question">
			{{ $value->question }}
			<span class="weight">
				(Weight: {{ $value->weight }}%) 
			</span>
			<span class="button"><a href="{{ url('delete_question/' . $value->id_detail_soal) }}">Delete</a></span>
			<div class="answer">
				<table>
					<tr>
						<td class="correct-answer">A. 100</td>
						<td>B. 1000</td>
					</tr>
					<tr>
						<td>C. 10</td>
						<td>D. 1</td>
					</tr>
					<tr>
						<td>E. 100000</td>
					</tr>
				</table>
			</div>
		</div>
		@endif
		@endforeach
	</div>
</div>
</div>

<div class="essay-wrapper">
<h3 style="text-align:center">Essay</h3>
<div class="form-wrapper">
	<form action="{{ url('store_question/' . $data->id_soal . '/essay') }}" method="POST">
	{{ csrf_field() }}
		<div>
			<input type="text" name="question" placeholder="Question">
		</div>
		<div>
			<input type="text" name="kj" placeholder="Keywords">
		</div>
		<div>
			<input type="number" name="weight" placeholder="Score Weight">
		</div>
		<div>
			<input type="submit" value="Insert Question" class="button">
		</div>
	</form>
</div>
<div>
	<div class="essay">
		@foreach ($data->detail_soal as $value)
		@if ($value->tipe == 'essay')
		<div class="question">
			{{ $value->question }}
			<span class="weight">
				(Weight: {{ $value->weight }}%) 
			</span>
			<span class="keywords">
				Keyword: {{ $value->kj }}
			</span>
			<span class="button"><a href="{{ url('delete_question/' . $value->id_detail_soal) }}">Delete</a></span>
		</div>
		@endif
		@endforeach
	</div>
</div>
@endsection