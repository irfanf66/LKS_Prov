@extends('layouts.app')
@section('content')
<div class="table-wrapper">
	<h3>Exam Statistic</h3>

	@php
	$dt = new \Carbon\Carbon();
	$dt::setToStringFormat('d F Y, h:i:s');
	$nilai = 0;
	@endphp
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Classroom</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th>Min</th>
			<th>Max</th>
			<th>Mean</th>
		</tr>
		@foreach (\App\Soal::all() as $val)

		@php
		$now = $dt::now();
		$start = $dt::createFromFormat("Y-m-d H:i:s", $val->start);
		$end = $dt::createFromFormat("Y-m-d H:i:s", $val->end);
		$nilai+=$val->nilai;
		$jawab = \App\Jawab::where('id_soal', $val->id_soal);
		@endphp
		<tr>
			<td>UTS Matematika</td>
			<td>10A</td>
			<td>10 Agustus 2017 09:00:00</td>
			<td>10 Agustus 2017 11:00:00</td>
			<td>{{ $jawab->min('nilai') }}</td>
			<td>{{ $jawab->max('nilai') }}</td>
			<td>{{ $jawab->avg('nilai') }}</td>
		</tr>
		@endforeach
	</table>
</div>
<div class="table-wrapper">
	<h3>Student Statistic</h3>
	<table border="1">
		<tr>
			<th>Classroom</th>
			<th>Min</th>
			<th>Max</th>
			<th>Mean</th>
			<th>Action</th>
		</tr>
		@foreach (\App\Kelas::all() as $val)
		<tr>
			@php
			$data = \App\Soal::leftJoin('jawab', 'soal.id_soal', 'jawab.id_soal')
				->where('id_kelas', $val->id_kelas);
			@endphp
			<td>{{ $val->nama_kelas }}</td>
			<td>{{ $data->min('nilai') ? $data->min('nilai') : 'Data Kosong' }}</td>
			<td>{{ $data->max('nilai') ? $data->min('nilai') : 'Data Kosong' }}</td>
			<td>{{ $data->avg('nilai') ? $data->min('nilai') : 'Data Kosong' }}</td>
			<td><a href="{{ url('statistic/' . $val->id_kelas) }}" class="button">View detail</a></td>
		</tr>
		@endforeach
		<tr>
			@php
			$data = \App\Jawab::all();
			@endphp
			<td>All Students</td>
			<td>{{ $data->min('nilai') }}</td>
			<td>{{ $data->max('nilai') }}</td>
			<td>{{ $data->avg('nilai') }}</td>
			<td><a href="{{ url('statistic/all') }}" class="button">View detail</a></td>
		</tr>
	</table>
</div>

@endsection