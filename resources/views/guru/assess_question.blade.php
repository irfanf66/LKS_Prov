@extends('layouts.app')
@section('content')
<div class="exam-data">
<div>{{ $data->soal->nama_soal }} kelas {{ $data->soal->kelas->nama_kelas }}</div>
<div>{{ date("d F Y H:i:s", strtotime($data->soal->start)) }} - {{ date("d F Y H:i:s", strtotime($data->soal->end)) }}</div>
</div>

<form method="POST" action="{{ url('/store_score/' . $data->id_jawab) }}">
{{ csrf_field() }}
@php
$totalskor = 0;
@endphp

<div class="multiple-choice-wrapper">
<h3 style="text-align:center">Multiple Choice</h3>
<div>
	<div class="multiple-choice">
		@foreach ($data->detail_jawab as $val)
		@if ($val->detail_soal->tipe == 'pg')
		<div class="question">
			{{ $val->detail_soal->question }}
			<span class="weight">
				(Weight: {{ $val->detail_soal->weight }}%) 
			</span>
			@php
			$skor = 0;
			if ($val->detail_soal->kj == $val->answer) {
				$skor = $val->detail_soal->weight;
			}
			$totalskor+=$skor;
			@endphp
			<div class="answer">
				<table>
					<tr>
						<td class="{{ $val->detail_soal->kj == 'a' ? 'correct-answer' : '' }} {{ $val->answer == 'a' ? 'correct-answer' : '' }}">A. {{ $val->detail_soal->optionA }}</td>
						<td class="{{ $val->detail_soal->kj == 'b' ? 'correct-answer' : '' }} {{ $val->answer == 'b' ? 'correct-answer' : '' }}">B. {{ $val->detail_soal->optionB }}</td>
					</tr>
					<tr>
						<td class="{{ $val->detail_soal->kj == 'c' ? 'correct-answer' : '' }} {{ $val->answer == 'c' ? 'correct-answer' : '' }}">C. {{ $val->detail_soal->optionC }}</td>
						<td class="{{ $val->detail_soal->kj == 'd' ? 'correct-answer' : '' }} {{ $val->answer == 'a' ? 'correct-answer' : '' }}">D. {{ $val->detail_soal->optionD }}</td>
					</tr>
					<tr>
						<td class="{{ $val->detail_soal->kj == 'e' ? 'correct-answer' : '' }} {{ $val->answer == 'e' ? 'correct-answer' : '' }}">E. {{ $val->detail_soal->optionE }}</td>
					</tr>
				</table>
			</div>
		</div>
		@endif
		@endforeach
	</div>
</div>
<div>
	Total score from multiple choice: {{ $totalskor }}
	<input type="hidden" name="skor_pg" value="{{ $totalskor }}">
</div>
</div>

<div class="essay-wrapper">
<h3 style="text-align:center">Essay</h3>
<div>
	@foreach ($data->detail_jawab as $val)
	@if ($val->detail_soal->tipe == 'essay')
	<div class="essay">
		<div class="question">
			{{ $val->detail_soal->question }}
			<span class="weight">
				(Weight: {{ $val->detail_soal->weight }}%) 
			</span>
			<span class="keywords">
				Keyword: {{ $val->detail_soal->kj }}
			</span>
		</div>
		<div class="answer">
			@php
			$x = explode('|', $val->detail_soal->kj);
			foreach ($x as $y) {
				$z = str_replace($y, '<span class="highlight-keyword">' . $y . '</span>', $val->answer);
			}
			@endphp
			{!! $z !!}
		</div>
	</div>
	<input type="number" name="skor[{{ $val->id_detail_soal }}]" placeholder="Score Essay">
	@endif
	@endforeach
</div>


<div style="margin-top:5%;text-align:center">
<input class="button" type="submit" value="Submit All Score">
</div>

</form>

@endsection