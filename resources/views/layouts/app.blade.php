
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">
</head>
<body>
<nav>
	<span>
		@if (Auth::user()->level == 0)
		<a href="{{ url('create_exam') }}">Create Exam</a>
		<a href="{{ url('assess_exam') }}">Assess Exam</a>
		<a href="{{ url('statistic') }}">Statistic</a>
		@else
		<a href="{{ url('answer_exam') }}">Answer Exam</a>
		<a href="{{ url('view_score') }}">View Score</a>
		@endif
		<a href="{{ url('logout') }}">Logout</a>
	</span>
</nav>

@yield('content')

</body>
</html>