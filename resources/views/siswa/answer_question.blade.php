@extends('layouts.app')
@section('content')
<div class="exam-data">
<div>{{ $data->nama_soal }} kelas {{ $data->kelas->nama_kelas }}</div>
<div>{{ date("d F Y H:i:s", strtotime($data->start)) }} - {{ date("d F Y H:i:s", strtotime($data->end)) }}</div>
</div>

<form method="POST" action="{{ url('store_answer/' . $data->id_soal) }}">
{{ csrf_field() }}
<div class="multiple-choice-wrapper">
<h3 style="text-align:center">Multiple Choice</h3>
<div>

	@php
	$data->detail_soal = $data->detail_soal->toArray();
	@endphp
	<div class="multiple-choice">
		@foreach ($data->detail_soal as $val)
		@if ($val['tipe'] == 'pg')
		<div class="question">
			{{ $val['question'] }}
			<span class="weight">
				(Weight: {{ $val['weight'] }}%) 
			</span>
			<div class="answer">
				<table>
					<tr>
						<td><input type="radio" name="answer[{{ $val['id_detail_soal'] }}]" value="a">{{ $val['optionA'] }}</td>
						<td><input type="radio" name="answer[{{ $val['id_detail_soal'] }}]" value="b">{{ $val['optionB'] }}</td>
					</tr>
					<tr>
						<td><input type="radio" name="answer[{{ $val['id_detail_soal'] }}]" value="c">{{ $val['optionC'] }}</td>
						<td><input type="radio" name="answer[{{ $val['id_detail_soal'] }}]" value="d">{{ $val['optionD'] }}</td>
					</tr>
					<tr>
						<td><input type="radio" name="answer[{{ $val['id_detail_soal'] }}]" value="e">{{ $val['optionE'] }}</td>
					</tr>
				</table>
			</div>
		</div>
		@endif
		@endforeach
	</div>
</div>
</div>
<div class="essay-wrapper">
<h3 style="text-align:center">Essay</h3>
<div>
	<div class="essay">
		@foreach ($data->detail_soal as $val)
		@if ($val['tipe'] == 'essay')
		<div class="question">
			{{ $val['question'] }}
			<span class="weight">
				(Weight: {{ $val['weight'] }}%) 
			</span>
		</div>
		<div class="answer">
			<textarea name="answer[{{ $val['id_detail_soal'] }}]"></textarea>
		</div>
		@endif
		@endforeach
	</div>
</div>

<div style="margin-top:5%;text-align:center">
<input type="submit" value="Submit Answers" class="button">
</div>

</form>
@endsection