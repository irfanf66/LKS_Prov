@extends('layouts.app')
@section('content')
<div class="table-wrapper">
	<h3>Your Score</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Start Exam Time</th>
			<th>End Exam Time</th>
			<th>Score</th>
		</tr>

		@php
		$dt = new \Carbon\Carbon();
		$dt::setToStringFormat('d F Y, h:i:s');
		$nilai = 0;
		@endphp

		@foreach ($jawab = \App\Jawab::where('id_user', Auth::user()->id_user)->get() as $val)

		@php
		$now = $dt::now();
		$start = $dt::createFromFormat("Y-m-d H:i:s", $val->soal->start);
		$end = $dt::createFromFormat("Y-m-d H:i:s", $val->soal->end);
		$nilai+=$val->nilai;
		@endphp

		<tr>
			<td>{{ $val->soal->nama_soal }}</td>
			<td>{{ $start }}</td>
			<td>{{ $end }}</td>
			<td>{{ $val->nilai }}</td>
		</tr>
		@endforeach
	</table>
	<div style="margin-top:1%">
	<div>Average Score: {{ $nilai / count($jawab) }}</div>
	<div>Classroom Rank: 2 of 40</div>
	<div>School Rank: 10 of 360</div>
	</div>
</div>
@endsection