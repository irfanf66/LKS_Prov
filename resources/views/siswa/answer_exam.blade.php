@extends('layouts.app')
@section('content')
<div class="table-wrapper">
	<h3>Answer Exam</h3>
	<table border="1">
		<tr>
			<th>Title</th>
			<th>Classroom</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th>Action</th>
		</tr>
		@php
		$dt = new \Carbon\Carbon();
		$dt::setToStringFormat('d F Y, h:i:s');
		@endphp

		@foreach (\App\Soal::where('id_kelas', \App\User::where('id_user', Auth::user()->id_user)->first()->siswa->id_kelas)->get() as $value)

		@php
		$now = $dt::now();
		$start = $dt::createFromFormat("Y-m-d H:i:s", $value->start);
		$end = $dt::createFromFormat("Y-m-d H:i:s", $value->end);

		$disabled = true;
		if ($now >= $start && $now <= $end)
			$disabled = false;

		@endphp

		<tr>
			<td>{{ $value->nama_soal }}</td>
			<td>{{ $value->kelas->nama_kelas }}</td>
			<td>{{ $start }}</td>
			<td>{{ $end }}</td>
			<td>
				<a href="{{ $disabled ? '#' : url('answer_question/' . $value->id_soal) }}" class="button {{ $disabled ? 'disabled' : '' }}">Answer</a>
			</td>
		</tr>

		@endforeach
	</table>
</div>
@endsection